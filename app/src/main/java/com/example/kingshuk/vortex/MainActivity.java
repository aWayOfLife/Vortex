package com.example.kingshuk.vortex;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;


import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class MainActivity extends AppCompatActivity {

    private ArrayList<Song> songList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SongAdapterRV mAdapter;
    CardView mNowPlaying;
    VisualizerView mVisualizerView;
    private MediaPlayer mMediaPlayer;
    private Visualizer mVisualizer;
    private Equalizer mEqualizer;
    FloatingActionButton mPlayPause, mPlayNext, mPlayPrevious;
    ImageView mPlayRepeat;
    boolean playStatus = false;
    int SessionId;
    Long albumID;
    TextView name, singer;
    SharedPreferences prefs;
    Uri songUri = null;
    ImageView mAlbumArt;
    BroadcastReceiver receiver, stopReceiver;
    SharedPreferences.Editor editor;
    private static String clientID = "25473098"; // Put your clientID here.
    private static String clientTag = "413792343F3F6F4E00095E35D38EE79E"; // Put your clientTag here.

    private static String apiKey = "1beddfe7dd678c59a28b1bc50e354393";

    DatabaseHelper myDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toast.makeText(this,"Create",Toast.LENGTH_SHORT).show();
        prefs = getSharedPreferences("PREFERENCES", MODE_PRIVATE);
        editor = getSharedPreferences("PREFERENCES", MODE_PRIVATE).edit();
        //prefs = getSharedPreferences("PREFERENCES", MODE_PRIVATE);

        try {
            mVisualizer.setEnabled(false);


        } catch (Exception e) {

        }

        setUpScreen();
        listeners();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //SessionId = intent.getIntExtra("SESSION ID", 0);
                //String k= String.valueOf(SessionId);
                // Toast.makeText(MainActivity.this, k, Toast.LENGTH_SHORT).show();
                //setupVisualizerFxAndUI(SessionId);
                // mVisualizer.setEnabled(true);
                // mEqualizer.setEnabled(true);

                final String song_name = intent.getStringExtra("SONG NAME");
                final String artist_name = intent.getStringExtra("ARTIST NAME");
                long albumID = intent.getLongExtra("ALBUM ID", 0);
                SessionId = intent.getIntExtra("SESSION ID", 0);


                //Toast.makeText(MainActivity.this,Long.toString(albumID),Toast.LENGTH_SHORT).show();

                name.setText(song_name);
                singer.setText(artist_name);


                Uri sArtworkUri = Uri
                        .parse("content://media/external/audio/albumart");


                Uri uri = ContentUris.withAppendedId(sArtworkUri, albumID);
                Picasso.with(MainActivity.this).load(uri)
                        .resize(720, dpToPx(135))
                        .centerCrop()
                        .placeholder(mAlbumArt.getDrawable())
                        .into(mAlbumArt, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                String Url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=1beddfe7dd678c59a28b1bc50e354393&artist=" + artist_name.replace(" ", "%20") + "&track=" + song_name.replace(" ", "%20") + "&format=json";
                                TestAsyncTask testAsyncTask = new TestAsyncTask(MainActivity.this, Url);
                                testAsyncTask.execute();
                            }
                        });


                /*else
                {
                    String Url= "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=1beddfe7dd678c59a28b1bc50e354393&artist="+artist_name.replace(" ","%20")+"&track="+song_name.replace(" ","%20")+"&format=json";
                    TestAsyncTask testAsyncTask = new TestAsyncTask(MainActivity.this, Url);
                    testAsyncTask.execute();
                }*/


                mPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
                setupVisualizerFxAndUI(SessionId);
                mVisualizer.setEnabled(true);
                mEqualizer.setEnabled(true);
            }
        };

        stopReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //stopService(new Intent(MainActivity.this, PlayerService.class));
                mPlayPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
            }
        };

    }


    void setUpScreen() {

        mNowPlaying = (CardView) findViewById(R.id.nowPlaying);
        mAlbumArt = (ImageView) findViewById(R.id.album_art);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mVisualizerView = (VisualizerView) findViewById(R.id.myvisualizerview);
        mPlayPause = (FloatingActionButton) findViewById(R.id.play_pause);
        mPlayPrevious = (FloatingActionButton) findViewById(R.id.play_previous);
        mPlayNext = (FloatingActionButton) findViewById(R.id.play_next);
        mPlayRepeat = (ImageView) findViewById(R.id.play_repeat);
        repeat_songpref();

        name = (TextView) findViewById(R.id.song_name);
        singer = (TextView) findViewById(R.id.song_singer);
        name.setSelected(true);
        singer.setSelected(true);

        prepareEntryData();

        mAdapter = new SongAdapterRV(songList);

        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(MainActivity.this);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    void updateFromSharedPrefs() {

        if (isMyServiceRunning(PlayerService.class) == true) {
            try {

                if (name.getText().equals(prefs.getString("SONG NAME", "Feels Like Coming Home")) == false) {
                    name.setText(prefs.getString("SONG NAME", "Feels Like Coming Home"));
                    singer.setText(prefs.getString("ARTIST NAME", "Jetta"));
                    albumID = prefs.getLong("ALBUM ID", 0);
                    SessionId = prefs.getInt("SESSION ID LAST", 0);
                    //Toast.makeText(MainActivity.this,Integer.toString(SessionId),Toast.LENGTH_SHORT).show();

                    Uri sArtworkUri = Uri
                            .parse("content://media/external/audio/albumart");


                    Uri uri = ContentUris.withAppendedId(sArtworkUri, albumID);
                    Picasso.with(MainActivity.this).load(uri)
                            .resize(720, dpToPx(135))
                            .centerCrop()
                            .placeholder(mAlbumArt.getDrawable())
                            .into(mAlbumArt, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    String Url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=1beddfe7dd678c59a28b1bc50e354393&artist=" + prefs.getString("ARTIST NAME", "Jetta").replace(" ", "%20") + "&track=" + prefs.getString("SONG NAME", "Feels Like Coming Home").replace(" ", "%20") + "&format=json";
                                    TestAsyncTask testAsyncTask = new TestAsyncTask(MainActivity.this, Url);
                                    testAsyncTask.execute();
                                }
                            });


                }

            } catch (Exception e) {

            }

            //setupVisualizerFxAndUI(0);
            //mVisualizer.setEnabled(true);
            //mEqualizer.setEnabled(true);
             /* AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //TODO your background code
                        setupVisualizerFxAndUI(SessionId);
                        mVisualizer.setEnabled(true);

                    }
                });*/

            setupVisualizerFxAndUI(SessionId);
            mVisualizer.setEnabled(true);
            mEqualizer.setEnabled(true);
            //Toast.makeText(MainActivity.this,"Enabled",Toast.LENGTH_SHORT).show();
            mPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
        } else {
            mPlayPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);

        }


    }

    void listeners() {


        mPlayRepeat.setClickable(true);
        mPlayRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String repeat_boolean = prefs.getString("PREFERENCES_REPEAT", "NO");
                if (repeat_boolean.equals("YES")) {
                    editor.putString("PREFERENCES_REPEAT", "NO");
                    mPlayRepeat.setColorFilter(MainActivity.this.getResources().getColor(R.color.appColorLight));
                } else {
                    editor.putString("PREFERENCES_REPEAT", "YES");
                    mPlayRepeat.setColorFilter(MainActivity.this.getResources().getColor(R.color.appColorWhite));

                }
                editor.apply();
            }
        });


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        mPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
                        startPlayer(view, position);
                    }

                    @Override
                    public void onLongItemClick(final View view, int position) {
                        // do whatever


                        final Song song = songList.get(position);
                        MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this)
                                .title("EDIT DETAILS")
                                .titleColorRes(R.color.appColor)
                                .customView(R.layout.edit_tag, true)
                                .positiveText("SAVE")
                                .negativeText("CANCEL")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        // TODO
                                        View rootView = dialog.getCustomView();
                                        EditText artistEdit = (EditText) rootView.findViewById(R.id.edit_artist);
                                        EditText trackEdit = (EditText) rootView.findViewById(R.id.edit_track);
                                        TextView artist = (TextView) view.findViewById(R.id.song_artist);
                                        TextView track = (TextView) view.findViewById(R.id.song_title);
                                        artist.setText(artistEdit.getText());
                                        track.setText(trackEdit.getText());
                                        song.setArtist(artistEdit.getText().toString());
                                        song.setTitle(trackEdit.getText().toString());
                                        int count = 0;
                                        final Cursor res = myDb.getAllData();

                                        if (res.getCount() != 0) {
                                            while (res.moveToNext()) {
                                                if (res.getString(4).equals(String.valueOf(song.getId()))) {
                                                    count++;
                                                    //myDb.updatedata(song.getGenre(),artistEdit.getText().toString(),trackEdit.getText().toString(),Long.toString(song.getId()),Long.toString(song.getAlbumID()));
                                                }
                                            }
                                        }

                                        if (count == 0) {
                                            //add
                                            boolean k = myDb.insertdata(song.getGenre(), trackEdit.getText().toString(), artistEdit.getText().toString(), Long.toString(song.getId()), Long.toString(song.getAlbumID()));
                                        } else {
                                            //update
                                            boolean k = myDb.updatedata(song.getGenre(), trackEdit.getText().toString(), artistEdit.getText().toString(), Long.toString(song.getId()), Long.toString(song.getAlbumID()));
                                        }

                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        // TODO
                                        Toast.makeText(MainActivity.this, "CANCELLED", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .show();

                        View rootView = dialog.getCustomView();
                        EditText artistEdit = (EditText) rootView.findViewById(R.id.edit_artist);
                        artistEdit.setText(song.getArtist());
                        EditText trackEdit = (EditText) rootView.findViewById(R.id.edit_track);
                        trackEdit.setText(song.getTitle());
                    }

                })
        );


        mPlayPause.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (isMyServiceRunning(PlayerService.class) == true) {
                    stopPlayer();
                    mVisualizer.setEnabled(false);
                    mEqualizer.setEnabled(false);
                    //mVisualizer.release();
                } else {
                    try {
                        Intent intent = new Intent(MainActivity.this, PlayerService.class);
                        intent.putExtra("CURRENT POSITION", prefs.getInt("CURRENT POSITION", 0));
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("arraylist", songList);
                        intent.putExtras(bundle);
                        mPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
                        playStatus = true;
                        startService(intent);
                    } catch (Exception e) {

                    }

                }
            }
        });


        mPlayPrevious.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (isMyServiceRunning(PlayerService.class) == true) {
                    stopService(new Intent(MainActivity.this, PlayerService.class));
                    mVisualizer.setEnabled(false);
                    mEqualizer.setEnabled(false);
                    //mVisualizer.release();

                    try {
                        Intent intent = new Intent(MainActivity.this, PlayerService.class);
                        intent.putExtra("CURRENT POSITION", prefs.getInt("CURRENT POSITION", 0) - 1);
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("arraylist", songList);
                        intent.putExtras(bundle);
                        mPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
                        playStatus = true;
                        startService(intent);
                    } catch (Exception e) {

                    }

                }
            }
        });

        mPlayNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (isMyServiceRunning(PlayerService.class) == true) {
                    stopService(new Intent(MainActivity.this, PlayerService.class));
                    mVisualizer.setEnabled(false);
                    mEqualizer.setEnabled(false);
                    //mVisualizer.release();

                    try {
                        Intent intent = new Intent(MainActivity.this, PlayerService.class);
                        intent.putExtra("CURRENT POSITION", prefs.getInt("CURRENT POSITION", 0) + 1);
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("arraylist", songList);
                        intent.putExtras(bundle);
                        mPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
                        playStatus = true;
                        startService(intent);
                    } catch (Exception e) {

                    }

                }
            }
        });

        mNowPlaying.isClickable();
        mNowPlaying.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

    }

    void repeat_songpref() //to check for repetition of songs
    {

        if (prefs.getString("PREFERENCES_REPEAT", "NO").equals("NO")) {

            mPlayRepeat.setColorFilter(MainActivity.this.getResources().getColor(R.color.appColorLight));

        } else {

            mPlayRepeat.setColorFilter(MainActivity.this.getResources().getColor(R.color.appColorWhite));

        }

    }

    private void setupVisualizerFxAndUI(int SessionId) {

        if (mVisualizer != null) {
            mVisualizer.release();
        }

        mVisualizer = new Visualizer(SessionId);
        mEqualizer = new Equalizer(0, SessionId);
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setScalingMode(Visualizer.SCALING_MODE_NORMALIZED);
        mVisualizer.setMeasurementMode(Visualizer.MEASUREMENT_MODE_PEAK_RMS);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        mVisualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    void prepareEntryData() {

        myDb = new DatabaseHelper(this);

        String ar[] = {"1", "2", "3", "4", "5"};
        ContentResolver musicResolver = getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
        //iterate over results if valid
        if (musicCursor != null && musicCursor.moveToFirst()) {
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int duration = musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int albumIDcolumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            //add songs to list


            do {

                int c = 0;
                String genre = "";
                double u = Math.random();
                if (u > 0 && u <= 0.20)
                    genre = ar[0];
                else if (u > 0.20 && u <= 0.40)
                    genre = ar[1];
                else if (u > 0.40 && u <= 0.60)
                    genre = ar[2];
                else if (u > 0.60 && u <= 0.80)
                    genre = ar[3];
                else
                    genre = ar[4];
                long thisId = musicCursor.getLong(idColumn);
                long albumID = musicCursor.getLong(albumIDcolumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                long d = musicCursor.getLong(duration);

                if (d > 30000) {
                    final Cursor cursor = myDb.getAllData();
                    if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                            String data = cursor.getString(4);
                            if (Long.parseLong(data) == thisId) {
                                c++;
                                //Toast.makeText(MainActivity.this, "YES", Toast.LENGTH_SHORT).show();
                                songList.add(new Song(cursor.getString(1), cursor.getString(2), cursor.getString(3), Long.parseLong(cursor.getString(4)), Long.parseLong(cursor.getString(5))));
                                break;
                            }
                            // do what ever you want here
                            cursor.moveToNext();
                        }
                    }
                    cursor.close();
                    if (c == 0) {

                        songList.add(new Song(genre, thisTitle, thisArtist, thisId, albumID));

                    }
                }
            } while (musicCursor.moveToNext());


            if (songList.size() > 0) {
                Collections.sort(songList, new Comparator<Song>() {
                    @Override
                    public int compare(final Song object1, final Song object2) {
                        return object1.getTitle().compareTo(object2.getTitle());
                    }
                });
            }
        }
    }


    void startPlayer(View view, int position) {
        try {


            Intent intent = new Intent(MainActivity.this, PlayerService.class);
            //intent.putExtra("SONG URI", songUri.toString());
            intent.putExtra("CURRENT POSITION", position);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("arraylist", songList);
            intent.putExtras(bundle);
            mPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
            playStatus = true;
            startService(intent);
        } catch (Exception e) {
        }
    }

    void stopPlayer() {
        //stopservice


        mPlayPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        // mVisualizer.setEnabled(false);
        stopService(new Intent(this, PlayerService.class));

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();


        updateFromSharedPrefs();
        LocalBroadcastManager.getInstance(this).registerReceiver((stopReceiver),
                new IntentFilter("SONG STOPPED")


        );

        //Toast.makeText(this,"Start",Toast.LENGTH_SHORT).show();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter("AUDIO DATA")


        );
    }

    @Override
    protected void onStop() {
        try {
            mVisualizer.setEnabled(false);
            mEqualizer.setEnabled(false);
            mVisualizer.release();
            mEqualizer.release();
        } catch (Exception e) {

        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(stopReceiver);
        exportDatabse("Vortex.db");
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this,"Resume",Toast.LENGTH_SHORT).show();
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public void exportDatabse(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "backupname.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    public class TestAsyncTask extends AsyncTask<Void, Void, String> {
        private Context mContext;
        private String mUrl;

        public TestAsyncTask(Context context, String url) {
            mContext = context;
            mUrl = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dynamictext = (TextView) findViewById(R.id.dynamictext);
        }

        @Override
        protected String doInBackground(Void... params) {
            String resultString = null;
            String uriString = null;
            resultString = getJSON(mUrl);
            //Toast.makeText(MainActivity.this,resultString.substring(0,10),Toast.LENGTH_SHORT).show();
            try {
                JSONObject root = new JSONObject(resultString);
                JSONObject trackObject = root.getJSONObject("track");
                JSONObject albumObject = trackObject.getJSONObject("album");
                JSONArray imageArray = albumObject.getJSONArray("image");
                JSONObject imageObject = imageArray.getJSONObject(2);

                uriString = imageObject.getString("#text");
                //Toast.makeText(MainActivity.this,uriString,Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return uriString;
        }

        @Override
        protected void onPostExecute(String strings) {
            super.onPostExecute(strings);
            //name.setText(strings);
            //Toast.makeText(MainActivity.this,strings,Toast.LENGTH_SHORT).show();
            try {
                final Uri imageuri = Uri.parse(strings);
                Picasso.with(MainActivity.this).load(imageuri)
                        .resize(720, dpToPx(135))
                        .centerCrop()
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .placeholder(mAlbumArt.getDrawable())
                        .into(mAlbumArt, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(MainActivity.this).load(imageuri)
                                        .resize(720, dpToPx(135))
                                        .centerCrop()
                                        .error(R.drawable.back_2)
                                        .placeholder(mAlbumArt.getDrawable())
                                        .into(mAlbumArt);
                            }
                        });
            } catch (Exception e) {
                mAlbumArt.setImageResource(R.drawable.back_2);
            }
        }

        private String getJSON(String url) {
            HttpURLConnection c = null;
            try {
                URL u = new URL(url);
                c = (HttpURLConnection) u.openConnection();
                c.connect();
                int status = c.getResponseCode();
                switch (status) {
                    case 200:
                    case 201:
                        BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();
                        return sb.toString();
                }

            } catch (Exception ex) {
                return ex.toString();
            } finally {
                if (c != null) {
                    try {
                        c.disconnect();
                    } catch (Exception ex) {
                        //disconnect error
                    }
                }
            }
            return null;
        }
    }
}
