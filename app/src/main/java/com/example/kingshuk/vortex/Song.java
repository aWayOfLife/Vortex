package com.example.kingshuk.vortex;

/*
 * This is demo code to accompany the Mobiletuts+ series:
 * Android SDK: Creating a Music Player
 * 
 * Sue Smith - February 2014
 */

import android.os.Parcel;
import android.os.Parcelable;

public class Song implements Parcelable{
	
	private String genre;
	private String title;
	private String artist;
    private long id,albumID;

    public Song(String genre, String title, String artist,long id, long albumID) {
        this.genre = genre;
        this.title = title;
        this.artist = artist;
        this.id=id;
        this.albumID=albumID;
    }

    public long getAlbumID() {
        return albumID;
    }


    public String getGenre() {
        return genre;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

   public long getId(){return id;}

    public Song() {
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @SuppressWarnings("unused")
    public Song(Parcel in) {
        this();
        readFromParcel(in);
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(genre);
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeLong(id);
        dest.writeLong(albumID);


    }
    private void readFromParcel(Parcel in) {
        this.genre = in.readString();
        this.title = in.readString();
        this.artist = in.readString();
        this.id = in.readLong();
        this.albumID = in.readLong();
    }

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

}
