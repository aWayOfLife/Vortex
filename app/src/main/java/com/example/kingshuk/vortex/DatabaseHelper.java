package com.example.kingshuk.vortex;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Kingshuk on 23-Apr-16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="Vortex.db";
    public static final String TABLE_NAME="VORTEX_ENTRIES";
    public static final String COL1="ID";
    public static final String COL2="GENRE";
    public static final String COL3="TITLE";
    public static final String COL4="ARTIST";
    public static final String COL5="SONG_ID";
    public static final String COL6="ALBUM_ID";



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " +
                        COL1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        COL2 + " TEXT NOT NULL , " +
                        COL3 + " TEXT NOT NULL , " +
                        COL4 + " TEXT NOT NULL , " +
                        COL5 + " TEXT NOT NULL , " +
                        COL6 + " TEXT );"

        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertdata(String genre, String title, String artist, String song_id, String album_id)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COL2,genre);
        contentValues.put(COL3,title);
        contentValues.put(COL4, artist);
        contentValues.put(COL5,song_id);
        contentValues.put(COL6, album_id);
        long result=db.insert(TABLE_NAME,null,contentValues);
        if(result==-1)
            return false;
        else
            return true;
    }

    public boolean updatedata(String genre, String title, String artist, String song_id, String album_id)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COL2,genre);
        contentValues.put(COL3,title);
        contentValues.put(COL4, artist);
        contentValues.put(COL5,song_id);
        contentValues.put(COL6, album_id);
        long result=db.update(TABLE_NAME, contentValues, COL5 + "=\"" + song_id + "\"", null);
        if(result==-1)
            return false;
        else
            return true;
    }
   /* public boolean deletedata(String id_date)
    {  SQLiteDatabase db=this.getWritableDatabase();
        return db.delete(TABLE_NAME, COL2 + "=\"" + id_date + "\"", null) > 0;
    }*/

    public Cursor getAllData()
    {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res=db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        return res;
    }

}
