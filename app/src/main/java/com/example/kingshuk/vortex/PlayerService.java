package com.example.kingshuk.vortex;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
//import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import android.support.v7.app.NotificationCompat;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;


public class PlayerService extends Service {
    public static final String EXTRA_PLAYLIST = "EXTRA_PLAYLIST";
    public static final String EXTRA_SHUFFLE = "EXTRA_SHUFFLE";
    private boolean isPlaying = false;
    MediaPlayer mediaPlayer;
    private Visualizer mVisualizer;
    private Equalizer mEqualizer;
    SharedPreferences.Editor editor;
    LocalBroadcastManager broadcaster ;
    SharedPreferences prefs;
    int position;
    ArrayList<Song> songList;MyReceiver mReceiver;

    public class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // do something

            if(intent.getAction().contentEquals("PREVIOUS"))
            {
                //Toast.makeText(PlayerService.this,"previous",Toast.LENGTH_SHORT).show();
                try {
                    play(--position);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(intent.getAction().contentEquals("PAUSE"))
            {
                //Toast.makeText(PlayerService.this,"pause",Toast.LENGTH_SHORT).show();
                Intent stopintent = new Intent("SONG STOPPED");

                //intent.putExtra("SESSION ID",id);
                //intent.putExtra("SONG NAME",song.getTitle().toString());
                //intent.putExtra("ARTIST NAME",song.getArtist().toString());
                //intent.putExtra("ALBUM ID",mAlbumID);

                broadcaster = LocalBroadcastManager.getInstance(PlayerService.this);
                broadcaster.sendBroadcast(stopintent);
                stop();
                PlayerService.this.stopSelf();
            }
            if(intent.getAction().contentEquals("NEXT"))
            {
               // Toast.makeText(PlayerService.this,"next",Toast.LENGTH_SHORT).show();
                try {
                    play(++position);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // constructor
        public MyReceiver(){

        }
    }


    @Override
    public void onCreate() {
      super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction("PREVIOUS");
        filter.addAction("PAUSE");
        filter.addAction("NEXT");
        mReceiver = new MyReceiver();
        registerReceiver(mReceiver, filter);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
       // String uri = intent.getExtras().getString("SONG URI");
           // Uri songUri = Uri.parse(uri);
            position = intent.getIntExtra("CURRENT POSITION",0) ;
            Bundle extras =intent.getExtras();
            songList  = extras.getParcelableArrayList("arraylist");





            if(position<0)
                position++;

            if(position==songList.size())
                position=0;

            play(position);

        } catch (IOException e) {
            e.printStackTrace();
        }



        return (START_NOT_STICKY);

    }



    @Override
    public IBinder onBind(Intent intent) {
        return (null);
    }

    public void play(int pos)throws IOException {
        if (!isPlaying) {
            Log.w(getClass().getName(), "Got to play()!");

            //save the one being played
            editor = getSharedPreferences("PREFERENCES", MODE_PRIVATE).edit();
            prefs = getSharedPreferences("PREFERENCES", MODE_PRIVATE);

            Song song = songList.get(pos);


            long mSongID=song.getId();
            //Toast.makeText(PlayerService.this,Long.toString(mSongID),Toast.LENGTH_SHORT).show();

            long mAlbumID=song.getAlbumID();

            Uri songUri= ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,mSongID);

            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(getApplicationContext(), songUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
            isPlaying = true;
            int id = mediaPlayer.getAudioSessionId();

            editor.putString("SONG NAME",song.getTitle().toString());
            editor.putString("ARTIST NAME", song.getArtist().toString());
            editor.putLong("ALBUM ID", mAlbumID);
            editor.putString("SONG URI", songUri.toString());
            editor.putInt("SESSION ID LAST",id);
            editor.putInt("CURRENT POSITION",pos);
            editor.apply();


            //lets play




            Intent intent = new Intent("AUDIO DATA");
            intent.putExtra("SESSION ID",id);
            intent.putExtra("SONG NAME",song.getTitle().toString());
            intent.putExtra("ARTIST NAME",song.getArtist().toString());
            intent.putExtra("ALBUM ID",mAlbumID);

            broadcaster = LocalBroadcastManager.getInstance(this);
            broadcaster.sendBroadcast(intent);





            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );



            Intent previous = new Intent();
            previous.setAction("PREVIOUS");
            PendingIntent prevPendingIntent = PendingIntent.getBroadcast(this, 1,previous, PendingIntent.FLAG_UPDATE_CURRENT);
            //mBuilder.addAction(R.drawable.calendar_v, "Yes", pendingIntentYes);

            //Maybe intent
            Intent pause = new Intent();
            pause.setAction("PAUSE");
            PendingIntent pausePendingIntent = PendingIntent.getBroadcast(this, 2, pause, PendingIntent.FLAG_UPDATE_CURRENT);
            //mBuilder.addAction(R.drawable.calendar_question, "Partly", pendingIntentMaybe);

            //No intent
            Intent next = new Intent();
            next.setAction("NEXT");
            PendingIntent nextPendingIntent = PendingIntent.getBroadcast(this, 3, next, PendingIntent.FLAG_UPDATE_CURRENT);
           // mBuilder.addAction(R.drawable.calendar_x, "No", pendingIntentNo);





           Notification notification = new NotificationCompat.Builder(this)
                    // Show controls on lock screen even when user hides sensitive content.
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setSmallIcon(R.drawable.ic_play_circle_filled_white_24dp)
                    // Add media control buttons that invoke intents in your media service
                    .addAction(R.drawable.ic_skip_previous_black_24dp, "PREVIOUS", prevPendingIntent) // #0
                    .addAction(R.drawable.ic_pause_black_24dp, "PAUSE", pausePendingIntent)  // #1
                    .addAction(R.drawable.ic_skip_next_black_24dp, "NEXT", nextPendingIntent)     // #2
                    // Apply the media style template
                   // .setStyle(new NotificationCompat.MediaStyle()
                           // .setShowActionsInCompactView(1))
                            //.setMediaSession(mMediaSession.getSessionToken()))
                    .setContentTitle(prefs.getString("SONG NAME", "Now Playing"))
                    .setContentText(prefs.getString("ARTIST NAME", "Unknown"))
                    .setContentIntent(resultPendingIntent).build();




            /*Notification notification=new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_play_circle_filled_white_24dp)
                    .setContentTitle(prefs.getString("SONG NAME", "Now Playing"))
                    .setContentText(prefs.getString("ARTIST NAME", "Unknown"))
                    .setContentIntent(resultPendingIntent).build();*/

            startForeground(2, notification);

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    try {

                        mediaPlayer.stop();
                        isPlaying=false;
                        if(position==songList.size()-1)
                        {
                            if(prefs.getString("PREFERENCES_REPEAT","NO").equals("NO"))
                                position=0;
                            play(position);
                        }
                        else {
                            if(prefs.getString("PREFERENCES_REPEAT","NO").equals("NO"))
                                play(++position);
                            else
                                play(position);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        else
        {
            stop();
            play(pos);
        }

    }

    private void stop() {
        if (isPlaying) {
            Log.w(getClass().getName(), "Got to stop()!");
            mediaPlayer.stop();
            mediaPlayer.reset();
            isPlaying = false;
            stopForeground(true);



        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mediaPlayer.stop();
        mediaPlayer.reset();
        unregisterReceiver(mReceiver);
    }



}
