package com.example.kingshuk.vortex;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Kingshuk on 25-Mar-16.
 */
public class SongAdapterRV extends RecyclerView.Adapter<SongAdapterRV.SongViewHolder> {
    private ArrayList<Song> songListP;

    FragmentManager manager;
    public static class SongViewHolder extends RecyclerView.ViewHolder {

        public TextView title, artist;
        FrameLayout container;
        ImageView genre;    Context mContext;
        public SongViewHolder(View view) {
            super(view);
            mContext= view.getContext();
            title = (TextView) view.findViewById(R.id.song_title);
            artist = (TextView) view.findViewById(R.id.song_artist);
            genre = (ImageView) view.findViewById(R.id.song_genre);
            container =(FrameLayout) view.findViewById(R.id.song_item_container);
        }
    }


    public SongAdapterRV(ArrayList<Song> songListP) {
        this.songListP = songListP;

    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_song, parent, false);

        return new SongViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        final Song song = songListP.get(position);
        holder.title.setText(song.getTitle());
        holder.artist.setText(song.getArtist());

        int g= Integer.parseInt(song.getGenre());

            if(g==1)
                holder.genre.setImageResource(R.drawable.rock);

            if(g==2)
                holder.genre.setImageResource(R.drawable.pop);

            if(g==3)
                holder.genre.setImageResource(R.drawable.metal);

            if(g==4)
                holder.genre.setImageResource(R.drawable.classical);

            if(g==5)
                holder.genre.setImageResource(R.drawable.electronic);



        }



    @Override
    public int getItemCount() {
        return songListP.size();
    }
}

